/**
 * Created by manuel on 20-04-15.
 */
"use strict";

//e
//document = window.document;
var Engine = require("famous/core/Engine");
var Surface = require("famous/core/Surface");
var Modifier = require("famous/core/Modifier");
var ContainerSurface = require("famous/surfaces/ContainerSurface");
var Scrollview = require("famous/views/Scrollview");

class App {


    constructor() {
        console.log('hello there!');
        this.createLayout();
    }

    createLayout() {

        var mainContext = Engine.createContext();

        var container = new ContainerSurface({
            size: [400, 400],
            properties: {
                overflow: 'hidden'
            }
        });

        var surfaces = [];
        var scrollview = new Scrollview();

        var temp;
        for (var i = 0; i < 100; i++) {
            temp = new Surface({
                size: [undefined, 50],
                content: 'I am surface: ' + (i + 1),
                classes: ['red-bg'],
                properties: {
                    textAlign: 'center',
                    lineHeight: '50px'
                }
            });

            temp.pipe(scrollview);
            surfaces.push(temp);
        }

        scrollview.sequenceFrom(surfaces);
        container.add(scrollview);

        mainContext.add(new Modifier({
            align: [.5, .5],
            origin: [.5, .5]
        })).add(container);
    }
}

module.exports = App;
